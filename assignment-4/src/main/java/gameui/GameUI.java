package gameui;

import card.Card;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class GameUI extends JFrame{
    private ArrayList<Card> cards = new ArrayList<Card>(0);
    private String winningSource;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private int attemp = 3;
    private int tries = 0;
    JButton playAgain;
    JPanel topPanel;
    JPanel bottomPanel;
    JLabel labelTemp;
    JLabel labelTry;
    Timer t;

    public GameUI(String resource, ArrayList<String> cardsName) throws IOException {
        ImageIcon closedCard = new ImageIcon(resource + "\\closed_card.png");
        cardsName.addAll(new ArrayList<String>(cardsName));
        winningSource = resource + "\\winning\\win_";

        // Buttoning
        for(int i = 0; i < cardsName.size(); i++){
            ImageIcon openedCard = new ImageIcon(resource + "\\" + cardsName.get(i) + ".png");
            Card c = new Card(cardsName.get(i), closedCard, openedCard);
            c.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae){
                    selectedCard = c;
                    doTurn();
                }
            });
            cards.add(c);
        }
        Collections.shuffle(cards);
        t = new Timer(750, new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                checkCards();
            }
        });
        t.setRepeats(false);

        // Layouting Top Panel cards
        GridLayout grid = new GridLayout(0, 6);
        topPanel = new JPanel();
        topPanel.setLayout(grid);
        for (Card c : cards){
            topPanel.add(c);
        }

        // Layouting Bottom Panel control
        bottomPanel = new JPanel();
        labelTry = new JLabel("Num of Tries: " + tries);
        bottomPanel.add(labelTry);
        playAgain = new JButton("Play Again");
        playAgain.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){ playAgain(); }
        });
        bottomPanel.add(playAgain);
        JButton giveUp = new JButton("Give up");
        giveUp.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){ giveUp(); }
        });
        bottomPanel.add(giveUp);
        labelTemp = new JLabel("Attemp left: " + attemp);
        bottomPanel.add(labelTemp);

        // Framing
        add(topPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.PAGE_END);
        setTitle("Match-Pair Memory Match");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600, 600);
    }

    public void doTurn(){
        if (c1 == null && c2 == null){
            c1 = selectedCard;
            c1.setSelected(true);
        }

        if (c1 != null && c1 != selectedCard && c2 == null){
            c2 = selectedCard;
            c2.setSelected(true);
            tries++;
            labelTry.setText("Num of Tries: " + tries);
            t.start();
        }
    }

    public void checkCards(){
        if (c1.getName().equals(c2.getName())){//match condition
            c1.setEnabled(false); //disables the button
            c2.setEnabled(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            if (this.isGameWon()){
                for(int i = 0; i < cards.size(); i++){
                    cards.get(i).setIcon(new ImageIcon(winningSource + (i+1) + ".png"));
                    cards.get(i).setEnabled(true);
                }
                Object[] options = {playAgain, "Exit"};
                JOptionPane.showOptionDialog(this, "You win!", "Winning!",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[1]);
                System.exit(0);
            }
        }

        else{
            c1.setSelected(false);
            c2.setSelected(false);
        }
        c1 = null; //reset c1 and c2
        c2 = null;
    }

    public void giveUp(){
        if(attemp >= 1){
            attemp--;
            labelTemp.setText("Attemp left: " + attemp);
            c1 = null;
            c2 = null;
            for(Card c : cards){ c.setSelected(true); }
            Timer time = new Timer(1500, new ActionListener(){
                public void actionPerformed(ActionEvent ae){
                    for(Card c : cards) {
                        if (!c.isMatched()) {
                            c.setSelected(false);
                            c.setEnabled(true);
                        }
                    }
                }
            });
            time.setRepeats(false);
            time.start();
        }else{ JOptionPane.showMessageDialog(this, "Don't give up! You can do it!"); }
    }

    public void playAgain(){
        attemp = 3;
        labelTemp.setText("Attemp left: " + attemp);
        tries = 0;
        labelTry.setText("Num of Tries: " + tries);
        Collections.shuffle(cards);
        topPanel.removeAll();
        for (Card c : cards){
            c.setSelected(false);
            c.setMatched(false);
            c.setEnabled(true);
            topPanel.add(c);
        }
        add(topPanel);
        getContentPane().invalidate();
        getContentPane().validate();
    }

    public boolean isGameWon(){
        for(Card c: this.cards){
            if (c.isMatched() == false){ return false; }
        }
        return true;
    }
}