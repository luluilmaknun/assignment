package card;

import javax.swing.*;
import java.awt.*;

public class Card extends JButton{
    private String name;
    private boolean selected;
    private boolean matched;
    private ImageIcon closed;
    private ImageIcon opened;

    public Card(String name, ImageIcon closed, ImageIcon opened){
        this.name = name;
        selected = false;
        matched = false;
        this.closed = closed;
        this.opened = opened;
        this.setIcon(closed);
        setBackground(Color.BLACK);
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return selected;
    }

    public boolean isMatched() {
        return matched;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        if(selected){
            this.setIcon(opened);
        }else{
            this.setIcon(closed);}
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }
}