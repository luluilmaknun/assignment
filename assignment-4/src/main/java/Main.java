import card.Card;
import gameui.GameUI;

import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws IOException {
        String resource = "D:\\Belajar\\DDP_Assignment\\assignment-4\\data";
        ArrayList<String> cardsName = new ArrayList<String>(){{
            add("anjing"); add("ayam"); add("babi"); add("beruang"); add("burung"); add("gurita");
            add("hermit"); add("ikan"); add("kambing"); add("keledai"); add("kelinci"); add("kura");
            add("monyet"); add("panda"); add("penguin"); add("rusa"); add("ular"); add("zebra");
        }};

        GameUI game = new GameUI(resource, cardsName);
        game.setVisible(true);
    }
}