import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    // You can add new variables or methods in this class

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int numOfCat = input.nextInt();
        input.nextLine();

        String[] specCat = input.nextLine().split(",");
        WildCat cat = new WildCat(specCat[0], Integer.parseInt(specCat[1]), Integer.parseInt(specCat[2]));
        TrainCar train = new TrainCar(cat);

        int remainCat = 1;
        for (int i = 0; i < numOfCat-1; i++){
            if (train.computeTotalWeight() <= THRESHOLD){
                specCat = input.nextLine().split(",");
                cat = new WildCat(specCat[0], Integer.parseInt(specCat[1]), Integer.parseInt(specCat[2]));
                train = new TrainCar(cat, train);
                remainCat += 1;
            }else{
                train.departCar(remainCat);
                specCat = input.nextLine().split(",");
                cat = new WildCat(specCat[0], Integer.parseInt(specCat[1]), Integer.parseInt(specCat[2]));
                train = new TrainCar(cat);
                remainCat = 1;
            }
        }

        train.departCar(remainCat);
    }
}
