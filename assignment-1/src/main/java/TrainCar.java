public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    WildCat cat;
    TrainCar next;

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        if( next == null){
            return cat.weight + EMPTY_WEIGHT;
        }else{
            return cat.weight + EMPTY_WEIGHT + next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        if (next == null){
            return cat.computeMassIndex();
        }else{
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }

    public String checkCategory(){
        double mass = computeTotalMassIndex();
        if(mass < 18.5){
            return "underweight";
        }else if(mass < 25){
            return "normal";
        }else if(mass < 30){
            return "overweight";
        }else{return "obese";}
    }

    public void departCar(int numOfCat){
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--"); printCar();
        System.out.format("\nAverage mass index of all cats: %.2f%n", (computeTotalMassIndex()/numOfCat));
        System.out.format("In average, the cats in the train are *%s*\n", checkCategory());
    }

    public void printCar() {
        if (next == null){
            System.out.print("(" + cat.name + ")");
        }else{
            System.out.print("(" + cat.name + ")--");
            next.printCar();
        }
    }
}
