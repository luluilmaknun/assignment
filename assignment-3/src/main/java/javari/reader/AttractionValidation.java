package javari.reader;

import javari.park.AnimalManager;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class AttractionValidation extends CsvReader {
    ArrayList<String> attrac = new ArrayList<String>();

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public AttractionValidation(Path file) throws IOException {
        super(file);
    }

    @Override
    public long countValidRecords() {
        for(String line : lines){
            String[] spec = line.split(COMMA);
            AnimalManager.addAttraction(spec[0], spec[1]);
            if(!attrac.contains(spec[1]) && (spec[1].equals("Circles of Fires") ||
                                            spec[1].equals("Dancing Animals") ||
                                            spec[1].equals("Counting Masters") ||
                                            spec[1].equals("Passionate Coders"))){
                attrac.add(spec[1]);
            }
        }
        return attrac.size();
    }

    @Override
    public long countInvalidRecords() {
        long counter = 0;
        for(String line : lines){
            String[] spec = line.split(COMMA);
            if(!attrac.contains(spec[1])){
                counter++;
            }
        }
        return counter;
    }
}
