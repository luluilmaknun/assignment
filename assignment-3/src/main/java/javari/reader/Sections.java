package javari.reader;

import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Mammals;
import javari.animal.Reptiles;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class Sections extends CsvReader {
    private ArrayList<String> validSections = new ArrayList<String>();
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public Sections(Path file) throws IOException {
        super(file);
    }

    @Override
    public long countValidRecords() {
        long counter = 0;
        for(String line: lines){
            String[] spec = line.split(COMMA);
            if(spec[1].equals("mammals") && spec[2].equals("Explore the Mammals")){
                Mammals.MAMMALS_LIST.add(spec[0]);
            }else if(spec[1].equals("aves") && spec[2].equals("World of Aves")){
                Aves.AVES_LIST.add(spec[0]);
            }else if(spec[1].equals("reptiles") && spec[2].equals("Reptillian Kingdom")){
                Reptiles.REPTILES_LIST.add(spec[0]);
            }
        }
        if(Mammals.MAMMALS_LIST.size() >= 0){counter++;}
        if(Aves.AVES_LIST.size() >= 0){counter++;}
        if(Reptiles.REPTILES_LIST.size() >= 0){counter++;}
        return counter;
    }

    @Override
    public long countInvalidRecords() {
        long counter = 0;
        for(String line: lines){
            String[] spec = line.split(COMMA);
            if(spec[1].equals("mammals") && !(spec[2].equals("Explore the Mammals"))){
                counter++;
            }else if(spec[1].equals("aves") && !(spec[2].equals("World of Aves"))){
                counter++;
            }else if(spec[1].equals("reptiles") && !(spec[2].equals("Reptillian Kingdom"))){
                counter++;
            }else{
                counter++;
            }
        }
        return counter;
    }

    public long countValidSection(){
        for(String line : lines){
            String[] spec = line.split(COMMA);
            if(!validSections.contains(spec[2]) && (spec[2].equals("World of Aves") ||
                                                    spec[2].equals("Explore the Mammals") ||
                                                    spec[2].equals("Reptillian Kingdom"))){
                validSections.add(spec[2]);
            }
        }
        return validSections.size();
    }

    public long countInvalidSection(){
        long counter = 0;
        for(String line : lines){
            String[] spec = line.split(COMMA);
            if(!validSections.contains(spec[2])){
                counter++;
            }
        }
        return counter;
    }

    public String getSection(){
        String output = "";
        for(int i = 1; i <= validSections.size(); i++){
            output += "\n" + i + ". " + validSections.get(i-1);
        }
        return output;
    }
}
