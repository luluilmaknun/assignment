package javari.reader;

import javari.animal.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class AnimalRecords extends CsvReader {
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public AnimalRecords(Path file) throws IOException {
        super(file);
    }

    @Override
    public long countValidRecords() {
        long counter = 0;
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(String line : lines){
            String[] spec = line.split(COMMA);
            int id = Integer.parseInt(spec[0]);
            if(!ids.contains(id) && spec.length == 8){
                ids.add(id);
                counter++;
            }
        }
        return counter;
    }

    @Override
    public long countInvalidRecords() {
        long counter = 0;
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(String line : lines){
            String[] spec = line.split(COMMA);
            int id = Integer.parseInt(spec[0]);
            if(!ids.contains(id) && spec.length == 8){
                ids.add(id);
            }else{counter++;}
        }
        return counter;
    }
}
