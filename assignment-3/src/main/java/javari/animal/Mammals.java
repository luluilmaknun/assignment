package javari.animal;

import java.util.ArrayList;

public class Mammals extends Animal {
    private boolean pregnant = false;
    public static ArrayList<String> MAMMALS_LIST = new ArrayList<String>(0);

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition){
        super(id, type, name, gender, length, weight, condition);
    }

    public void setPregnant(boolean pregnant) {
        this.pregnant = pregnant;
    }

    public boolean isPregnant(){
        return pregnant;
    }

    public boolean specificCondition(){
        if(isPregnant()){
            return false;
        }else if(getType().equals("Lion") && getGender() == Gender.FEMALE){
            return false;
        }
        return true;
    }

    public static String printAnimal(){
        String output = "\n--Explore the Mammals--";
        for(int i = 1; i <= MAMMALS_LIST.size(); i++){
            output += "\n" + i + ". " + MAMMALS_LIST.get(i-1);
        }
        output += "\nPlease choose your preferred animals (type the number): ";
        return output;
    }
}
