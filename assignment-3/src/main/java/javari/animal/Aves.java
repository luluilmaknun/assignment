package javari.animal;

import java.util.ArrayList;

public class Aves extends Animal {
    boolean layingEgg = false;
    public static ArrayList<String> AVES_LIST = new ArrayList<String>(0);

    public Aves(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition){
        super(id, type, name, gender, length, weight, condition);
    }

    public void setLayingEgg(boolean layingEgg) {
        this.layingEgg = layingEgg;
    }

    public boolean isLayingEgg() {
        return layingEgg;
    }

    public boolean specificCondition(){
        if(isLayingEgg()){
            return false;
        }
        return true;
    }

    public static String printAnimal(){
        String output = "\n--World of Aves--";
        for(int i = 1; i <= AVES_LIST.size(); i++){
            output += "\n" + i + ". " + AVES_LIST.get(i-1);
        }
        output += "\nPlease choose your preferred animals (type the number): ";
        return output;
    }
}
