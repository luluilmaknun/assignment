package javari.animal;

import java.util.ArrayList;

public class Reptiles extends Animal {
    private boolean tame;
    public static ArrayList<String> REPTILES_LIST = new ArrayList<String>(0);

    public Reptiles(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition){
        super(id, type, name, gender, length, weight, condition);

    }

    public void setTame(boolean tame) {
        this.tame = tame;
    }

    public boolean isTame(){
        return tame;
    }

    public boolean specificCondition(){
        if(!tame){
            return false;
        }
        return true;
    }

    public static String printAnimal(){
        String output = "\n--Reptillian Kingdom--";
        for(int i = 1; i <= REPTILES_LIST.size(); i++){
            output += "\n" + i + ". " + REPTILES_LIST.get(i-1);
        }
        output += "\nPlease choose your preferred animals (type the number): ";
        return output;
    }
}
