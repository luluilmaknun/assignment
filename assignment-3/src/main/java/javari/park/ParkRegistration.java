package javari.park;

import java.util.ArrayList;

public class ParkRegistration implements Registration {
    static int id = 1;
    private String name;
    private int objId;
    private ArrayList<SelectedAttraction> selectedAttraction = new ArrayList<SelectedAttraction>(0);

    public ParkRegistration(String name){
        this.name = name;
        objId = id;
        id++;
    }
    @Override
    public int getRegistrationId() {
        return id;
    }

    @Override
    public String getVisitorName() {
        return name;
    }

    @Override
    public String setVisitorName(String name) {
        return null;
    }

    @Override
    public ArrayList<SelectedAttraction> getSelectedAttractions() {
        return selectedAttraction;
    }

    @Override
    public void addSelectedAttraction(SelectedAttraction selected) {
        selectedAttraction.add(selected);
    }
}
