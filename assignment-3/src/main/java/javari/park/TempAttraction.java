package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TempAttraction implements SelectedAttraction {
    String name;
    String type;
    ArrayList<Animal> performers;

    public TempAttraction(String name, String type, ArrayList<Animal> performers){
        this.name = name;
        this.type = type;
        this.performers = performers;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public ArrayList<Animal> getPerformers() {
        return performers;
    }
}
