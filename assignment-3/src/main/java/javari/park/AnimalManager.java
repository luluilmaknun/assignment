package javari.park;

import javari.animal.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AnimalManager {
    private static ArrayList<Animal> ANIMAL_LIST = new ArrayList<Animal>(0);
    public static HashMap<String, ArrayList<String>> SHOWABLE_ANIMAL = new HashMap<String, ArrayList<String>>();
    public static HashMap<String, ArrayList<String>> ATTRACTION_LIST = new HashMap<String, ArrayList<String>>();
    public static HashMap<String, ArrayList<Animal>> ATTRACTION_ANIMAL = new HashMap<String, ArrayList<Animal>>();

    public static void setShowableAnimal(){
        for(Animal animal : ANIMAL_LIST){
            ArrayList<String> attList = ATTRACTION_LIST.get(animal.getType());
            for(String att : attList){
                ArrayList<Animal> a = ATTRACTION_ANIMAL.get(att);
                if(a == null){
                    a = new ArrayList<Animal>();
                    ATTRACTION_ANIMAL.put(att, a);
                }
                a.add(animal);
            }
        }
    }

    public static void addAttraction(String type, String attraction){
        ArrayList<String> attList = ATTRACTION_LIST.get(type);
        if(attList == null){
            attList = new ArrayList<String>();
            ATTRACTION_LIST.put(type, attList);
        }
        attList.add(attraction);
    }

    public static void addShowable(Animal animal){
        ArrayList<String> showList = SHOWABLE_ANIMAL.get(animal.getType());
        if(showList == null){
            showList = new ArrayList<String>();
            SHOWABLE_ANIMAL.put(animal.getType(), showList);
        }
        if(animal.isShowable()){
            showList.add(animal.getName());
        }
    }

    public static String printAttraction(String type){
        String output = "";
        if(SHOWABLE_ANIMAL.get(type) != null){
            output += "\n--" + type + "--"+
                    "\nAttractions by " + type + ":";
            ArrayList<String> attraction = ATTRACTION_LIST.get(type);
            for(int i = 1; i <= attraction.size(); i++){
                output += "\n" + i + ". " + attraction.get(i-1);
            }
            output += "\nPlease choose your preferred attractions (type the number): ";
        }else{
            output += "\nUnfortunately no " + type + " can perform any attraction, please choose other animals";
        }
        return output;
    }

    public static void getRecords(List<String> lines){
        for(String line : lines){
            String[] spec = line.split(",");
            if(Mammals.MAMMALS_LIST.contains(spec[1])){
                Animal animal = new Mammals(Integer.parseInt(spec[0]), spec[1], spec[2],
                        Gender.parseGender(spec[3]), Double.parseDouble(spec[4]),
                        Double.parseDouble(spec[5]), Condition.parseCondition(spec[7]));
                if(spec[7].equals("pregnant")){
                    ((Mammals) animal).setPregnant(true);
                }
                if(animal.isShowable()){
                    addShowable(animal);
                }
                ANIMAL_LIST.add(animal);
            }else if(Aves.AVES_LIST.contains(spec[1])){
                Animal animal = new Aves(Integer.parseInt(spec[0]), spec[1], spec[2],
                        Gender.parseGender(spec[3]), Double.parseDouble(spec[4]),
                        Double.parseDouble(spec[5]), Condition.parseCondition(spec[7]));
                if(spec[7].equals("laying eggs")){
                    ((Aves) animal).setLayingEgg(true);
                }
                if(animal.isShowable()){
                    addShowable(animal);
                }
                ANIMAL_LIST.add(animal);
            }else if(Reptiles.REPTILES_LIST.contains(spec[1])) {
                Animal animal = new Reptiles(Integer.parseInt(spec[0]), spec[1], spec[2],
                        Gender.parseGender(spec[3]), Double.parseDouble(spec[4]),
                        Double.parseDouble(spec[5]), Condition.parseCondition(spec[7]));
                if(spec[7].equals("tame")){
                    ((Reptiles) animal).setTame(true);
                }
                if(animal.isShowable()){
                    addShowable(animal);
                }
                ANIMAL_LIST.add(animal);
            }
        }
    }
}
