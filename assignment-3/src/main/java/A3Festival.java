import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import javari.writer.*;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.nio.file.Path;

public class A3Festival {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);

        System.out.print("Welcome to Javari Park Festival - Registration Service!\n" +
                "\n... Opening default section database from data. ... File not found or incorrect file!\n" +
                "\nPlease provide the source data path: ");

        String paths = input.next();
        Path categories_path = Paths.get(paths + "\\animals_categories.csv");
        Path attractions_path = Paths.get(paths + "\\animals_attractions.csv");
        Path records_path = Paths.get(paths + "\\animals_records.csv");

        Sections categoriesR = new Sections(categories_path);
        AttractionValidation attractionV = new AttractionValidation(attractions_path);
        AnimalRecords animalR = new AnimalRecords(records_path);

        long validAnimalC = categoriesR.countValidRecords();
        System.out.format("\n\n... Loading... Success... System is populating data...\n" +
                        "\nFound %d valid sections and %d invalid sections" +
                        "\nFound %d valid attractions and %d invalid attractions" +
                        "\nFound %d valid animal categories and %d invalid animal categories" +
                        "\nFound %d valid animal records and %d invalid animal records\n",
                categoriesR.countValidSection(), categoriesR.countInvalidSection(),
                attractionV.countValidRecords(), attractionV.countInvalidRecords(),
                validAnimalC, categoriesR.countInvalidRecords(),
                animalR.countValidRecords(), animalR.countInvalidRecords());

        AnimalManager.getRecords(animalR.getLines());
        AnimalManager.setShowableAnimal();

        System.out.format("\nWelcome to Javari Park Festival - Registration Service!\n" +
                        "\nPlease answer the questions by typing the number. Type # if you want to return to the previous menu\n");
        menu1(validAnimalC, categoriesR.getSection(), paths);
    }

    public static void menu1(long valid, String section, String paths) throws IOException {
        Scanner input = new Scanner(System.in);
        System.out.format("\nJavari Park has %d sections: %s" +
                "\nPlease choose your preffered section (type the number): ", valid, section);
        String choosedS = input.next();
        if(choosedS.equals("#")){
            menu1(valid, section, paths);
        }
        menu2(Integer.parseInt(choosedS), paths);
    }

    public static void menu2(int num, String paths) throws IOException{
        Scanner input = new Scanner(System.in);
        ArrayList<ArrayList<String>> typeList = new ArrayList<ArrayList<String>>();
        typeList.add(Mammals.MAMMALS_LIST);
        typeList.add(Aves.AVES_LIST);
        typeList.add(Reptiles.REPTILES_LIST);
        if(num == 1){
            System.out.print(Mammals.printAnimal());
        } else if(num == 2){
            System.out.print(Aves.printAnimal());
        } else if(num == 3){
            System.out.print(Reptiles.printAnimal());
        }
        String choosedA = input.next();
        if(choosedA.equals("#")) {
            menu2(num, paths);
        }
        menu3(Integer.parseInt(choosedA), typeList.get(num-1), paths);
    }

    public static void menu3(int num, ArrayList<String> typeList, String paths) throws IOException{
        Scanner input = new Scanner(System.in);
        String animalType = typeList.get(num-1);
        System.out.print(AnimalManager.printAttraction(animalType));
        String choosedAt = input.next();
        if(choosedAt.equals("#")) {
            menu3(num, typeList, paths);
        }
        int number = Integer.parseInt(choosedAt);
        String attraction = AnimalManager.ATTRACTION_LIST.get(animalType).get(number-1);
        menu4(attraction, animalType, paths);
    }

    public static void menu4(String attraction, String animalType, String paths) throws IOException{
        Scanner input = new Scanner(System.in);
        ArrayList<Animal> canAttract = new ArrayList<Animal>(0);
        for(Animal anim : AnimalManager.ATTRACTION_ANIMAL.get(attraction)){
            if(anim.getType().equals(animalType)){
                canAttract.add(anim);
            }
        }

        TempAttraction selected = new TempAttraction(attraction, animalType, canAttract);

        System.out.print("\nWow, one more step,\n" +
                "please let us know your name: ");
        String visitorName = input.nextLine();

        ParkRegistration visitor = new ParkRegistration(visitorName);
        visitor.addSelectedAttraction(selected);

        ArrayList<String> animalsName = AnimalManager.SHOWABLE_ANIMAL.get(animalType);
        String showAble = "";
        int count = 0;
        int size = animalsName.size();
        for(String animalName : animalsName){
            count++;
            showAble += animalName.substring(0,1).toUpperCase() + animalName.substring(1);
            if(count != size){
                showAble += ", ";
            }
        }

        System.out.format("\n\nYeay, final check!\n" +
                "Here is your data, and the attraction you chose:\n" +
                "Name: %s\n" +
                "Attractions: %s -> %s\n" +
                "With: %s\n", visitorName, attraction, animalType, showAble);

        System.out.print("\nIs the data correct? (Y/N): ");
        String isyes = input.next();
        if(isyes.equals("Y")){
            System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N):");
            String yes = input.next();
            if(yes.equals("Y")){
                Sections categoriesR = new Sections(Paths.get(paths + "\\animals_categories.csv"));
                menu1(categoriesR.countValidRecords(), categoriesR.getSection(), paths);
            }
            RegistrationWriter.writeJson(visitor, Paths.get(paths));
        }
    }
}
