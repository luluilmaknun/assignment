import cages.*;
import animals.*;
import cagemanager.*;
import animalmanager.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Officer{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        AnimalManager animanager = new AnimalManager();
        CageManager cagemanager = new CageManager();
        String[] animalString = new String[]{"cat", "lion", "eagle", "parrot", "hamster"};

        System.out.println("Welcome to Javari Park!\n" +
                            "Input number of animals");

        for (String animal : animalString){
            System.out.format("%s: ", animal); int num = input.nextInt(); input.nextLine();
            if (num > 0){
                System.out.format("Provide the information of %s(s): \n", animal);
                String info = input.nextLine();
                switch(animal){
                    case "lion":animanager.lionManager(num, info); break;
                    case "eagle":animanager.eagleManager(num, info); break;
                    case "parrot":animanager.parrotManager(num, info); break;
                    case "hamster":animanager.hamsterManager(num, info); break;
                    default:animanager.catsManager(num, info); break;
                }
            }
        }

        System.out.println("Animals has been succesfully recorded!\n\n=============================================");
        System.out.print("Cage arrangement: ");

        ArrayList<ArrayList<Cages>> cages = cagemanager.placeCages(animanager.getAnimals());

        for(ArrayList<Cages> cage : cages){
            if(cage.size() > 0){
                cagemanager.beforeArrange(cage);
                System.out.println("\nlocation: " + cage.get(0).getIndoor());
                System.out.format("level 3: %s\n", cagemanager.printCage(cagemanager.cagez_3));
                System.out.format("level 2: %s\n", cagemanager.printCage(cagemanager.cagez_2));
                System.out.format("level 1: %s\n", cagemanager.printCage(cagemanager.cagez_1));

                System.out.println("\nAfter rearrangement...");
                ArrayList<Cages> cagez_1 = CageManager.arrangeCage(cagemanager.cagez_2);
                ArrayList<Cages> cagez_2 = CageManager.arrangeCage(cagemanager.cagez_1);
                ArrayList<Cages> cagez_3 = CageManager.arrangeCage(cagemanager.cagez_3);
                System.out.format("level 3: %s\n", cagemanager.printCage(cagez_1));
                System.out.format("level 2: %s\n", cagemanager.printCage(cagez_2));
                System.out.format("level 1: %s\n", cagemanager.printCage(cagez_3));
                cagemanager.clearAll();
            }
        }

        System.out.println("\nNUMBER OF ANIMALS:" +
                           "\ncat:" + cages.get(0).size() +
                           "\nlion:" + cages.get(1).size() +
                           "\nparrot:" + cages.get(2).size() +
                           "\neagle:" + cages.get(3).size() +
                           "\nhamster:" + cages.get(4).size());

        System.out.println("\n=============================================");

        do{
            try{
                System.out.println("Which animal you want to visit?\n(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
                int choosed = input.nextInt(); input.nextLine();
                if(choosed == 99){
                    break;
                }else if(choosed == 1){
                    System.out.print("Mention the name of cat you want to visit: ");
                    String nama = input.nextLine();
                    Cats visitedOne = animanager.getCats(nama);
                    visitedOne.visit();
                    int dos = input.nextInt();
                    visitedOne.doSomething(dos);
                }else if(choosed == 2){
                    System.out.print("Mention the name of eagle you want to visit: ");
                    String nama = input.nextLine();
                    Eagle visitedOne = animanager.getEagle(nama);
                    visitedOne.visit();
                    int dos = input.nextInt();
                    visitedOne.doSomething(dos);
                }else if(choosed == 3){
                    System.out.print("Mention the name of hamster you want to visit: ");
                    String nama = input.nextLine();
                    Hamsters visitedOne = animanager.getHamster(nama);
                    visitedOne.visit();
                    int dos = input.nextInt();
                    visitedOne.doSomething(dos);
                }else if(choosed == 4){
                    System.out.print("Mention the name of parrot you want to visit: ");
                    String nama = input.nextLine();
                    Parrots visitedOne = animanager.getParrot(nama);
                    visitedOne.visit();
                    int dos = input.nextInt();
                    visitedOne.doSomething(dos);
                }else{
                    System.out.print("Mention the name of lion you want to visit: ");
                    String nama = input.nextLine();
                    Lion visitedOne = animanager.getLion(nama);
                    visitedOne.visit();
                    int dos = input.nextInt();
                    visitedOne.doSomething(dos);
                }
                System.out.println("Back to the office!");
            }catch(NullPointerException r){System.out.println("There is no cat with that name! Back to the office!");}

        }while(true);
    }
}
