package cagemanager;
import cages.*;
import animals.*;
import animalmanager.*;
import java.util.ArrayList;

public class CageManager {
    public ArrayList<Cages> cagez_1 = new ArrayList<Cages>();
    public ArrayList<Cages> cagez_2 = new ArrayList<Cages>();
    public ArrayList<Cages> cagez_3 = new ArrayList<Cages>();
    ArrayList<Cages> catCages = new ArrayList<Cages>();
    ArrayList<Cages> lionCages = new ArrayList<Cages>();
    ArrayList<Cages> eagleCages = new ArrayList<Cages>();
    ArrayList<Cages> hamsterCages = new ArrayList<Cages>();
    ArrayList<Cages> parrotCages = new ArrayList<Cages>();

    public ArrayList<ArrayList<Cages>> placeCages(ArrayList<Animal[]> animals){
        ArrayList<ArrayList<Cages>> cages = new ArrayList<ArrayList<Cages>>();
        cages.add(catCages); cages.add(lionCages); cages.add(eagleCages); cages.add(hamsterCages); cages.add(parrotCages);
        int i = 0;
        for(ArrayList<Cages> cage : cages){
            try{
                for (Animal specAnimal : animals.get(i)){
                    cage.add(new Cages(specAnimal.getType(), specAnimal.isPet(), specAnimal));
                }i++;
            }catch(NullPointerException r){i++;}
        }
        return cages;
    }

    public void beforeArrange(ArrayList<Cages> cages){
        int size = cages.size();
        int i = size / 3; int j;
        if(size % 3 == 2){
            j = i + size / 3 + 1;
        }else{
            i = size / 3; j = i + size / 3;
        }

        if(size == 1){
            cagez_1.add(cages.get(0));
        }else if(size == 2){
            cagez_1.add(cages.get(0));
            cagez_2.add(cages.get(1));
        }else if(size == 3){
            cagez_1.add(cages.get(0));
            cagez_2.add(cages.get(1));
            cagez_3.add(cages.get(2));
        }else{
            for(int a = 0; a < cages.size(); a++){
                if(a < i){
                    cagez_1.add(cages.get(a));
                }else if(a < j){
                    cagez_2.add(cages.get(a));
                }else{
                    cagez_3.add(cages.get(a));
                }
            }
        }
    }

    public static ArrayList<Cages> arrangeCage(ArrayList<Cages> cages){
        ArrayList<Cages> cagez = new ArrayList<Cages>();
        int num = cages.size();
        for(int i = 0; i < num; i++){
            cagez.add(cages.get(num - i - 1));
        }
        return cagez;
    }

    public String printCage(ArrayList<Cages> cages){
        String tulisan = "";
        for(Cages cage : cages){
            tulisan += cage.getAnimal().getName() + " (" + cage.getAnimal().getLength() + " - " + cage.getAnimal().getType() + "), ";
        }
        return tulisan;
    }

    public void clearAll(){
        cagez_1.clear(); cagez_2.clear(); cagez_3.clear();
    }
}
