package animalmanager;
import cages.*;
import animals.*;
import cagemanager.*;
import java.util.ArrayList;

public class AnimalManager{
    Cats[] catz; Lion[] lionz; Eagle[] eaglez; Hamsters[] hamsterz; Parrots[] parrotz;

    public void catsManager(int num, String info){
        String[] catInfo = info.split(",");
        catz = new Cats[num];
        for(int i = 0; i < num; i++){
            String nama = catInfo[i].split("\\|")[0];
            int length = Integer.parseInt(catInfo[i].split("\\|")[1]);
            catz[i] = new Cats(nama, length, true);
        }
    }

    public void lionManager(int num, String info){
        String[] lionInfo = info.split(",");
        lionz = new Lion[num];
        for(int i = 0; i < num; i++){
            String nama = lionInfo[i].split("\\|")[0];
            int length = Integer.parseInt(lionInfo[i].split("\\|")[1]);
            lionz[i] = new Lion(nama, length, false);
        }
    }

    public void eagleManager(int num, String info){
        String[] eagleInfo = info.split(",");
        eaglez = new Eagle[num];
        for(int i = 0; i < num; i++){
            String nama = eagleInfo[i].split("\\|")[0];
            int length = Integer.parseInt(eagleInfo[i].split("\\|")[1]);
            eaglez[i] = new Eagle(nama, length, false);
        }
    }

    public void hamsterManager(int num, String info){
        String[] hamsterInfo = info.split(",");
        hamsterz = new Hamsters[num];
        for(int i = 0; i < num; i++){
            String nama = hamsterInfo[i].split("\\|")[0];
            int length = Integer.parseInt(hamsterInfo[i].split("\\|")[1]);
            hamsterz[i] = new Hamsters(nama, length, true);
        }
    }

    public void parrotManager(int num, String info){
        String[] parrotInfo = info.split(",");
        parrotz = new Parrots[num];
        for(int i = 0; i < num; i++){
            String nama = parrotInfo[i].split("\\|")[0];
            int length = Integer.parseInt(parrotInfo[i].split("\\|")[1]);
            parrotz[i] = new Parrots(nama, length, true);
        }
    }

    public Cats getCats(String nama){
        for(Cats cat : catz){
            if(cat.getName().equals(nama)){
                return cat;
            }
        }
        return null;
    }

    public Lion getLion(String nama){
        for(Lion lion : lionz){
            if(lion.getName().equals(nama)){
                return lion;
            }
        }
        return null;
    }

    public Eagle getEagle(String nama){
        for(Eagle eagle : eaglez){
            if(eagle.getName().equals(nama)){
                return eagle;
            }
        }
        return null;
    }

    public Parrots getParrot(String nama){
        for(Parrots parrot : parrotz){
            if(parrot.getName().equals(nama)){
                return parrot;
            }
        }
        return null;
    }

    public Hamsters getHamster(String nama){
        for(Hamsters hamster : hamsterz){
            if(hamster.getName().equals(nama)){
                return hamster;
            }
        }
        return null;
    }

    public ArrayList<Animal[]> getAnimals(){
        ArrayList<Animal[]> animals = new ArrayList<Animal[]>();

        animals.add(catz);
        animals.add(lionz);
        animals.add(eaglez);
        animals.add(parrotz);
        animals.add(hamsterz);

        return animals;
    }
}
