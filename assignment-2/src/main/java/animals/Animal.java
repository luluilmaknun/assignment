package animals;

public class Animal{
    protected String name;
    protected int length;
    protected boolean isPet;
    protected String cagesType;

    public Animal(String name, int length, boolean isPet){
        this.name = name;
        this.length = length;
        this.isPet = isPet;
    }

    public boolean isPet(){
        return isPet;
    }

    public String getType(){
        return cagesType;
    }

    public String getName(){
        return name;
    }

    public int getLength(){
        return length;
    }
}
