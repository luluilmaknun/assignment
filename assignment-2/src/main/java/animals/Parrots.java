package animals;
import java.util.Scanner;

public class Parrots extends Animal{
    public Parrots(String name, int length, boolean isPet){
        super(name, length, isPet);

        if(length < 45){
            cagesType = "A";
        }else if(length < 60){
            cagesType = "B";
        }else{
            cagesType = "C";
        }
    }

    public void fly(){
        System.out.println(name + " makes a voice: FLYYYY...");
    }

    public void imitate(String words){
        System.out.println(name + " says: " + words.toUpperCase());
    }

    public void imitate(){
        System.out.println(name + " says: HM?");
    }

    public void visit(){
        System.out.println("You are visiting " + name + " (parrot) now, what would you like to do?");
        System.out.println("1: Order to fly 2: Do conversation");
    }

    public void doSomething(int i){
        Scanner input = new Scanner(System.in);

        if (i == 1){
            System.out.println("Parrot " + name + " flies!");
            fly();
        }else if (i == 2){
            System.out.print("You say: ");
            String say = input.nextLine();
            imitate(say);
        }else{
            imitate();
        }
    }
}
