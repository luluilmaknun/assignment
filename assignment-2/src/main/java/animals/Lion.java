package animals;

public class Lion extends Animal{
    public Lion(String name, int length, boolean isPet){
        super(name, length, isPet);

        if(length < 75){
            cagesType = "A";
        }else if(length < 90){
            cagesType = "B";
        }else{
            cagesType = "C";
        }
    }

    public void hunt(){
        System.out.println(name + " makes a voice: err....");
    }

    public void brushed(){
        System.out.println(name + " makes a voice: Hauhhmm!");
    }

    public void disturbed(){
        System.out.println(name + " makes a voice: HAUHHMM!!");
    }

    public void visit(){
        System.out.println("You are visiting " + name + " (lion) now, what would you like to do?");
        System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
    }

    public void doSomething(int i){
        if (i == 1){
            System.out.println("Lion is hunting.");
            hunt();
        }else if (i == 2){
            System.out.println("Clean the lion's mane");
            brushed();
        }else if (i == 3){
            disturbed();
        }else{
            System.out.println("You do nothing...");
        }
    }
}
