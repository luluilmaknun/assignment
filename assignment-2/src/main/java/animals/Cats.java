package animals;
import java.util.Random;

public class Cats extends Animal{
    public Cats(String name, int length, boolean isPet){
        super(name, length, isPet);

        if(length < 45){
            cagesType = "A";
        }else if(length < 60){
            cagesType = "B";
        }else{
            cagesType = "C";
        }
    }

    public void brushed(){
        System.out.println(name + " makes a voice: Nyaaan...");
    }

    public void cuddled(){
        String[] voices = new String[]{"Miaaaw...", "Purrr...", "Mwaw!", "Mwaaawr!"};
        Random randomizer = new Random();
        String voice = voices[randomizer.nextInt(voices.length)];
        System.out.println(name + " makes a voices: " + voice);
    }

    public void visit(){
        System.out.println("You are visiting " + name + " (cat) now, what would you like to do?");
        System.out.println("1: Brush the fur 2: Cuddle");
    }

    public void doSomething(int i){
        if (i == 1){
            System.out.println("Time to clean " + name + "'s fur");
            brushed();
        }else if (i == 2){
            cuddled();
        }else{
            System.out.println("You do nothing...");
        }
    }
}
