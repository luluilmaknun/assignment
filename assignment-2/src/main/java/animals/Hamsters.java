package animals;

public class Hamsters extends Animal{
    public Hamsters(String name, int length, boolean isPet){
        super(name, length, isPet);

        if(length < 45){
            cagesType = "A";
        }else if(length < 60){
            cagesType = "B";
        }else{
            cagesType = "C";
        }
    }

    public void gnaw(){
        System.out.println(name + " makes a voice: ngkkrit.. ngkkrrriiit");
    }

    public void runInWheel(){
        System.out.println(name + " makes a voice: trrr... trrr...");
    }

    public void visit(){
        System.out.println("You are visiting " + name + " (hamster) now, what would you like to do?");
        System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
    }

    public void doSomething(int i){
        if (i == 1){
            gnaw();
        }else if (i == 2){
            runInWheel();
        }else{
            System.out.println("You do nothing...");
        }
    }
}
