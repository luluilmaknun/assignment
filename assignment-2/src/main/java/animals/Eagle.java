package animals;

public class Eagle extends Animal{
    public Eagle(String name, int length, boolean isPet){
        super(name, length, isPet);

        if(length < 75){
            cagesType = "A";
        }else if(length < 90){
            cagesType = "B";
        }else{
            cagesType = "C";
        }
    }

    public void fly(){
        System.out.println(name + " makes a voice: Kwaakk...\nYou hurt!");
    }

    public void visit(){
        System.out.println("You are visiting " + name + " (eagle) now, what would you like to do?");
        System.out.println("1: Order to fly");
    }

    public void doSomething(int i){
        if (i == 1){
            fly();
        }else{
            System.out.println("You do nothing...");
        }
    }
}
