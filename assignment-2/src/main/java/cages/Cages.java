package cages;
import animals.*;

public class Cages{
    public String type;
    boolean isIndoor;
    Animal anim;

    public Cages(String type, boolean isIndoor, Animal anim){
        this.type = type;
        this.isIndoor = isIndoor;
        this.anim = anim;
    }

    public String getIndoor(){
        if(isIndoor){
            return "indoor";
        }
        return "outdoor";
    }

    public String getType(){
        return type;
    }

    public Animal getAnimal(){
        return anim;
    }
}
